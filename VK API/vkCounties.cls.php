<?php
class vkCountries
{

    public $array;

    public function __construct()
    {

    }

    public function vkGetInfo($method, $options = array(), $token = '')
    {
        $params = http_build_query($options);
        $url = 'https://api.vk.com/method/' . $method . '?' . $params . '&access_token=' . $token;
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);

        $response = curl_exec($curl);
        curl_close($curl);
        $this->array = json_decode($response);

    }
}