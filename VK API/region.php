<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Регионы</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
require 'vkCounties.cls.php';
$idc = $_GET['idc'];
$countriesOptions = array(
    'need_all' => 1,
    'country_id'=>$idc,
    'count' => 20
);
$data = new vkCountries();
$data->vkGetInfo('database.getRegions', $countriesOptions);
if (count($data->array->response)!=0) {
    echo '<ul class="nav nav-pills nav-stacked">';
    foreach ($data->array->response as $key => $val) {

        echo '<li><a href="city.php?idr=' . $val->region_id . '&idc=' . $idc . '">' . $val->title . '</a></li>';
    }
    echo '</ul><a href="index.php" class="btn btn-primary" role="button">Главная</a>';
} else  {
    $data->vkGetInfo('database.getCities', $countriesOptions);
    echo '<ul class="nav nav-pills nav-stacked">';
    foreach ($data->array->response as $key=>$val){
        echo '<li><a href="school.php?ids='.$val->cid.'">'.$val->title.'</a></li>';
    }
    echo '</ul><a href="index.php" class="btn btn-primary" role="button">Главная</a>';
}
?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
